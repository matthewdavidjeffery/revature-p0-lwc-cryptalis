## Project 0 - 1

This will be the first part of an ongoing and more extensive project. Keep this in 
mind as the foundation you build here will need to support future endeavors. 

You will determine a business you want to model. This can be an actual company or 
one you create.

## Lightning Web Components Open Source

You will use the LWC OSS framework to create a webpage for the company you've chosen. 
This webpage should consist of multiple Lightning web components that respond to user 
input. Additionally, one or more components must make use of conditional rendering.

## Styling

Your webpage must be well styled with a uniform design. 

## Git & Github

Your code should be routinely pushed to a Github repository that your trainer 
has access to.

## Project 0 Part 3

## Process Automation 

You will need to declaratively program automation for your chosen company that 
demonstrates your complete understanding of Salesforce process automation tools. 

These topics include 
* Workflows
* Process Builder
* Approval Processes
* Flows
* Validation Rules

> For this, you need to have at least two examples of each used in distinct ways. 

## SDLC 

Display your understanding of the Software Development Life Cycle by participating with your team in the correct usage of the Scrumban framework for Agile. 

## Presentation

You will present your completed project to the class. I will be expecting a powertpoint or google slides presetation as well as a live demo of 
your ORG and LWC OSS site. You should aim for 50/50 slides and demo. T
