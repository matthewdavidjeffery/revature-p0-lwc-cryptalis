import '@lwc/synthetic-shadow';
import { createElement } from 'lwc';
import CrtsApp from 'crts/app';

const app = createElement('crts-app', { is: CrtsApp });
// eslint-disable-next-line @lwc/lwc/no-document-query
document.querySelector('#main').appendChild(app);
