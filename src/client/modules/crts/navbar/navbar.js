import {LightningElement, api, track} from 'lwc'

export default class navbar extends LightningElement {
    @api currentrole = "landing";


    get isMarketing() {
        if (this.currentrole == "marketing") return true;
        return false;
    }

    get isAuthUser() {
        if (this.currentrole == "marketing") return true;
        return false;
    }

    get isAdmin() {
        if(this.currentrole == "admin") return true;
        return false;
    }

    dropdownToggle(e)
    {
        e.currentTarget.classList.toggle('slds-is-open');
    } 

    handleLoginClick(e) {

        this.dispatchEvent(new CustomEvent("loginsuccessful", {detail: "marketing"}));
       // console.log("Login event initiated...");

    }

    handleLogoutClick(e) {
        this.dispatchEvent(new CustomEvent("logoutsuccessful", {detail: this.currentrole}));
    }

    clickBlogLink(e) {
        this.dispatchEvent(new CustomEvent("linkclick", {detail: "blog"}));
    }

    clickLanding(e) {
        this.dispatchEvent(new CustomEvent("linkclick", {detail: "landing"}));
    }
}