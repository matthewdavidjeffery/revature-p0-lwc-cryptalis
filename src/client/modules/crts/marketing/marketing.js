import {LightningElement, api} from 'lwc'

export default class marketing extends LightningElement {
    currentMarketingBlurb = 0;
    
    marketingBlurbs = {
         "current": [
                    {
                        "title": "Cryptalis Cloud",
                        "subtitle": "Cloud Crypto Wallet Management",
                        "bodytext": "Cryptalis provides world class crypto services <em>without the world-class fees.",
                        "tags": "intro sticky frontpage",
                        "entrydate": "2022-01-25"
                    }, 
                    {
                        "title": "Secure, Easy Wallet Management",
                        "subtitle": "Your growing wealth, at a glance",
                        "bodytext": "Utilizing a very robust, cloud-based tech stack, Cryptalis Cloud provides everything you need to track, buy and sell currency and tokens on the most popular crypto currencies of today.",
                        "tags": "general frontpage",
                        "entrydate": "2022-01-25"   
                    }      
        ]
    };

    cycleMarketingBlurb()
    {
        this.currentMarketingBlurb++;
        if (this.currentMarketingBlurb > this.marketingBlurbs.length - 1)
        {
            this.currentMarketingBlurb = 0;
        }
    }

}