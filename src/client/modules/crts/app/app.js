import { LightningElement, api, track } from 'lwc';

export default class App extends LightningElement {


    // TODO: Put app state into an imported JS file for clarity

    appStates = {
        landing: "landing",
        login: "login",
        marketing_list: "marketing_list",
        marketing_detail: "marketing_detail",
        wallet_list: "wallet_list",
        wallet_detail: "wallet_detail"
    }

    appRoles = {
        nonuser: "notauth",
        user: "user",
        marketing: "marketing",
        admin: "admin"
    }

    @track currentUser = this.appRoles.nonuser;
    @track currentState = this.appStates.landing;

    // App state getters for conditional rendering
    get isLanding() {
       return (this.currentState == "landing")
    }
    // App state getters for conditional rendering
    get isBlog() {
        return (this.currentState == "blog")
    }

    get isLogin() {
        return (this.currentState === "login");
    }

    get isMarketingList() {
        return (this.currentState === "marketing_list");
    }

    get isMarketingDetail() {
        return (this.currentState === "marketing_detail");
    }

    get isWalletList() {
        return (this.currentState === "wallet_list");
    }

    get isWalletDetail() {
        return (this.currentState === "wallet_detail");
    }

    handleLogin(e)
    {
        this.currentUser = e.detail;
        //console.log("User " + e.detail + "logged in.");
    }

    handleLogout(e)
    {
        this.currentUser = "landing";
        //console.log("User " + e.detail + "logged out.");
    }

    handleLinkClick(e) {
        this.currentState = e.detail;
    }


}
